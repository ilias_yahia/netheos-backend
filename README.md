# Nehteos - Backend Challenge :  
  
Ce projet contient ma  solution au backend challenge de Netheos  
Choix des technologies :  
  
- Java 8  
- Apache Maven 3.5  
- Spring Boot 2.0.5.RELEASE
- Spring Security 5  
- Hibernate 5 ( BDD embarquée de type H2)  
  
J’ai choisi le framework Spring Boot car il permet de créer des API REST robustes facilement.

Spring fournit aussi un autre framework que j'ai utilisé sur le projet, Spring Security, qui permet de sécuriser les différents end-points de l’API REST en utilisant des tokens JWT.  
  
## Installation  
  

    git clone https://gitlab.com/ilias_yahia/netheos-backend.git
    mvn clean package 

 
  
Cette commande permet de générer le war qui sera  déployé sur un serveur Tomcat dont une image docker est déjà configurée et  qui permet de créer et lancer un conteneur docker avec  le projet dans Tomcat .  
Pour builder l'image docker :  
  

    docker build -t netheos-backend .  
    docker run --name netheos-challenge -p 8080:8080 -d netheos-backend 

## Authentification à l'API REST :

 
|End Point| Sécurisé | Accès |
|--|--|--|
| /login | Non | Tous |
| /register | Non | Tous |
| /faq/{id} | Oui | Utilisateur authentifié (ADMIN/USER) |
| /faq | Oui | Utilisateur authentifié (ADMIN) |
| /faq/create | Oui | Utilisateur authentifié (ADMIN) |
| /faq/create | Oui | Utilisateur authentifié (ADMIN) |
| /faq/search | Oui | Utilisateur authentifié (ADMIN/USER) |
| /users | Oui | Utilisateur authentifié (ADMIN) |
| /users/{id} | Oui | Utilisateur authentifié (USER) |

Pour récupérer le token d'authentification :

***Pour Admin :***

    curl -H "Content-Type: application/json" -d '{"username":"admin","password":"admin"}' http://localhost:8080/login

***Réponse :***

    {"token":"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6IlJPTEVfQURNSU4iLCJpYXQiOjE1Mzg0Njg2NTEsImV4cCI6MTUzODQ3MjI1MX0.kZlCLsOtWRno4dpHZog0i5mIucOEDWqW1PGPt9NH4yU"}

***Pour Utilisateur :***

    curl -H "Content-Type: application/json" -d '{"username":"user","password":"user"}' http://localhost:8080/login

***Réponse :***  

    {"token":"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwic2NvcGVzIjoiUk9MRV9VU0VSIiwiaWF0IjoxNTM4NDY4NzY2LCJleHAiOjE1Mzg0NzIzNjZ9.s53m9kih30cLooiPKmd0RWEQznjwTB86HBO3JigAy70"}

## Tester l'API REST :  
  
Voir le fichier PostMan associé : Netheos - Backend Challenge.postman_collection.json