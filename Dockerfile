FROM tomcat:8.5.15-jre8

ADD tomcat-conf/tomcat-users.xml /usr/local/tomcat/conf/
ADD tomcat-conf/context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
ADD target/netheos.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"]

EXPOSE 8080