package com.netheos.backendchallenge.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class FAQ {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String responseLabel;
    @Column
    private String questionLabel;
    @OneToMany(mappedBy = "faq", cascade = CascadeType.ALL)
    private List<Tag> tags;
}
