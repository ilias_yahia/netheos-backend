package com.netheos.backendchallenge.web;


import com.netheos.backendchallenge.domain.FAQ;
import com.netheos.backendchallenge.dto.FAQDto;
import com.netheos.backendchallenge.service.FAQService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FAQController {

    private final FAQService faqService;

    public FAQController(FAQService faqService) {
        this.faqService = faqService;
    }

    @GetMapping("/faq/{id}")
    @PreAuthorize("hasRole('USER')")
    ResponseEntity<?> getByFAQById(@RequestParam Long id) {
        FAQDto result = faqService.findById(id);
        if (result != null) return ResponseEntity.ok(result);
        else return ResponseEntity.notFound().build();
    }


    @GetMapping("/faq")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity<?> getAllFAQs() {
        List<FAQDto> result = faqService.findAll();
        if (result != null) return ResponseEntity.ok(result);
        else return ResponseEntity.notFound().build();
    }

    @PostMapping("/faq/create")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity<?> createFAQ(@RequestBody FAQDto body) {
        FAQDto result = faqService.createFAQ(body);
        if (result != null) return ResponseEntity.ok(result);
        else return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @GetMapping("/faq/search")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    ResponseEntity<?> searchFaq(@RequestParam("keyword") String word) {
        List<FAQDto> result = faqService.findFAQContaining(word);
        if (result != null && result.size() > 0) return ResponseEntity.ok(result);
        else return ResponseEntity.notFound().build();
    }

}
