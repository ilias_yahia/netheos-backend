package com.netheos.backendchallenge.service.impl;

import com.netheos.backendchallenge.data.FAQRepository;
import com.netheos.backendchallenge.domain.FAQ;
import com.netheos.backendchallenge.domain.Tag;
import com.netheos.backendchallenge.dto.FAQDto;
import com.netheos.backendchallenge.dto.TagDto;
import com.netheos.backendchallenge.service.FAQService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FAQServiceImpl implements FAQService {

    private final FAQRepository faqRepository;

    public FAQServiceImpl(FAQRepository faqRepository) {
        this.faqRepository = faqRepository;
    }

    @Override
    public FAQDto findById(Long id) {
        return mapToFAQDto(faqRepository.findById(id).get());
    }

    @Override
    public List<FAQDto> findByResponseLabel(String responseLabel) {
        return faqRepository.findAllByResponseLabel(responseLabel)
                .stream()
                .map(faq -> mapToFAQDto(faq))
                .collect(Collectors.toList());
    }

    @Override
    public List<FAQDto> findByQuestionLabel(String questionLabel) {
        return faqRepository.findAllByQuestionLabel(questionLabel)
                .stream()
                .map(faq -> mapToFAQDto(faq))
                .collect(Collectors.toList());
    }

    @Override
    public List<FAQDto> findFAQContaining(String word) {
        // Continuation de la solution 1 pour la User Story 3
        // On combine les deux recherche
//        List<FAQ> responsesContainingWord = faqRepository.findAllByResponseLabelContainingIgnoreCase(word);
//        List<FAQ> questionsContainingWord = faqRepository.findAllByQuestionLabelContainingIgnoreCase(word);
//
//        return Stream.concat(responsesContainingWord.stream(),questionsContainingWord.stream())
//                .map(faq -> mapToFAQDto(faq))
//                .collect(Collectors.toList());

        // Ou bien on peut utiliser du JPQL (solution 2)
        return faqRepository.findFAQContaining(word)
                .stream()
                .map(faq -> mapToFAQDto(faq))
                .collect(Collectors.toList());
    }

    @Override
    public List<FAQDto> findAll() {
        return faqRepository.findAll()
                .stream()
                .map(faq -> mapToFAQDto(faq))
                .collect(Collectors.toList());
    }

    @Override
    public FAQDto createFAQ(FAQDto faqDto) {
        return mapToFAQDto(faqRepository.save(mapToFAQ(faqDto)));
    }

    private FAQ mapToFAQ(FAQDto faqDto) {
        FAQ faq = new FAQ();
        faq.setQuestionLabel(faqDto.getQuestionLabel());
        faq.setResponseLabel(faqDto.getResponseLabel());
        faq.setTags(faqDto.getTags().stream().map(tagDto -> mapToTag(tagDto, faq)).collect(Collectors.toList()));
        return faq;
    }

    private Tag mapToTag(TagDto tagDto, FAQ faq) {
        Tag tag = new Tag();
        tag.setDescription(tagDto.getDescription());
        tag.setFaq(faq);
        return tag;
    }

    private FAQDto mapToFAQDto(FAQ faq) {
        FAQDto faqDto = new FAQDto();
        faqDto.setQuestionLabel(faq.getQuestionLabel());
        faqDto.setResponseLabel(faq.getResponseLabel());
        faqDto.setTags(faq.getTags().stream().map(tag -> mapToTagDto(tag)).collect(Collectors.toList()));
        return faqDto;
    }

    private TagDto mapToTagDto(Tag tag) {
        TagDto tagDto = new TagDto();
        tagDto.setDescription(tag.getDescription());
        return tagDto;
    }
}
