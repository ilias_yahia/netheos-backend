package com.netheos.backendchallenge.service;

import com.netheos.backendchallenge.domain.User;
import com.netheos.backendchallenge.dto.UserDto;

import java.util.List;

public interface UserService {
    User save(UserDto user);
    List<User> findAll();
    void delete(long id);
    User findOne(String username);
    User findById(Long id);
}
