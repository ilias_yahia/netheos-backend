package com.netheos.backendchallenge.service;

import com.netheos.backendchallenge.domain.FAQ;
import com.netheos.backendchallenge.dto.FAQDto;

import java.util.List;

public interface FAQService {
    FAQDto findById(Long id);
    FAQDto createFAQ(FAQDto faqDto);
    List<FAQDto> findByResponseLabel(String responseLabel);
    List<FAQDto> findByQuestionLabel(String questionLabel);
    List<FAQDto> findFAQContaining(String word);
    List<FAQDto> findAll();
}
