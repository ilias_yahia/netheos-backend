package com.netheos.backendchallenge.data;

import com.netheos.backendchallenge.domain.FAQ;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Queue;

public interface FAQRepository extends JpaRepository<FAQ, Long> {
    Optional<FAQ> findById(Long id);
    List<FAQ> findAllByResponseLabel(String responseLabel);
    List<FAQ> findAllByQuestionLabel(String questionLabel);

    // Solution 1 pour la Uer Story 3
    List<FAQ> findAllByQuestionLabelContainingIgnoreCase(String searchWord);
    List<FAQ> findAllByResponseLabelContainingIgnoreCase(String searchWord);

    // Solution 2 por la User Story 3
    @Query("select f from FAQ f where " +
            "lower(f.questionLabel) like lower(concat('%',:word,'%')) " +
            "or lower(f.responseLabel) like lower(concat('%',:word,'%'))")
    List<FAQ> findFAQContaining(@Param("word") String word);
}
