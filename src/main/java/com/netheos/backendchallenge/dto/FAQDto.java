package com.netheos.backendchallenge.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class FAQDto {
    private String responseLabel;
    private String questionLabel;
    private List<TagDto> tags;
}
