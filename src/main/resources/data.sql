drop table if exists role;
drop table if exists user;
drop table if exists user_roles;

create table role (id int not null, description varchar(255), name varchar(255), primary key (id));
create table user (id int not null, age integer, password varchar(255), salary int, username varchar(255), primary key (id));
create table user_roles (user_id int not null, role_id int not null, primary key (user_id, role_id));

alter table user_roles add constraint role_id_fk foreign key (role_id) references role (id);
alter table user_roles add constraint user_id_fk foreign key (user_id) references user (id);

INSERT INTO user (id, username, password) VALUES (1, 'admin', '$2a$10$6MFeDXzTu9pMsQU.RWKY9.iDNYSaBSCPGA1WFf9dhAmS2oGqzakpa');
INSERT INTO user (id, username, password) VALUES (2, 'user', '$2a$10$wtsaMLZ.lYwlt9UftTUWOOmkmld190SIbQZnVvb9uG5WNMkCPZ9da');

INSERT INTO role (id, description, name) VALUES (1, 'admin role', 'ADMIN');
INSERT INTO role (id, description, name) VALUES (2, 'user role', 'USER');

INSERT INTO user_roles (user_id, role_id) VALUES (1, 1);
INSERT INTO user_roles (user_id, role_id) VALUES (2, 2);